export const state = () => ({
    user: null
  })
  
export const mutations = {
    add (state, user) {
      state.user = user
    },
    remove (state) {
      state.user = null
    }
}